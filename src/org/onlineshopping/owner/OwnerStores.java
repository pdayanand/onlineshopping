package org.onlineshopping.owner;

public class OwnerStores {
    private ProductStore products = new ProductStore();
    private BrandStore brands = new BrandStore();

    public  OwnerStores(ProductStore products, BrandStore brands) {
        this.products = products;
        this.brands = brands;
    }

    public ProductStore getProducts() {
        return products;
    }

    public void setProducts(ProductStore products) {
        this.products = products;
    }

    public BrandStore getBrands() {
        return brands;
    }

    public void setBrands(BrandStore brands) {
        this.brands = brands;
    }
}
