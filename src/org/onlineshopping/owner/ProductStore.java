package org.onlineshopping.owner;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class ProductStore {
    private Map<Integer, ProductEntity> products = new HashMap<>();

    private ProductEntity tempProduct (int i, Brand brand) {
        ProductEntity productEntity = new ProductEntity(i,
                "Product name " + i, i ,brand,   i * 5, 10 * i, ProductType.CLOTH );


        return productEntity;
    }
    public void initialize(int counter, Brand brand) {
        for(int i = counter; i < counter + 10; i ++)
            addProduct(tempProduct(i, brand));
    }

    public void addProduct ( ProductEntity product) {
        products.put(product.getId(), product);
    }

    public void removeProduct (ProductEntity product) {
        products.remove(product.getId());
    }

    public ProductEntity getProduct (int id) {
        return products.get(id);
    }

    public String viewProductsAsString() {
        return products.values().stream().map(i -> i.getName()).collect(Collectors.joining(",\n"));
    }
}
