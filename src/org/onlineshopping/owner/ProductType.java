package org.onlineshopping.owner;

public enum ProductType {
    CLOTH,
    WALLET,
    BAG,
    SHOE,
    WATCH
}
