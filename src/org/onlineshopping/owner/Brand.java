package org.onlineshopping.owner;

public class Brand {
    private int brandid;
    private String name;
    private String desc;

    public Brand(int brandid, String name, String desc) {
        this.brandid = brandid;
        this.name = name;
        this.desc = desc;
    }


    public int getBrandid() {
        return brandid;
    }

    public void setBrandid(int brandid) {
        this.brandid = brandid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }



}
