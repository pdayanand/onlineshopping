package org.onlineshopping.owner;

public class ProductEntity {
    private int id;
    private String name;
    private int color;
    private Brand brand;
    private int quantity;
    private float price;
    private ProductType type;

    public ProductEntity(int id, String name, int color, Brand brand, int quantity, float price, ProductType type) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.brand = brand;
        this.quantity = quantity;
        this.price = price;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public Brand getBrandid() {
        return brand;
    }

    public void setBrandid(Brand brand) {
        this.brand = brand;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
