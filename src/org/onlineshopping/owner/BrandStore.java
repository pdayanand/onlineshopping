package org.onlineshopping.owner;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class BrandStore {
    private Map<Integer, Brand> brands;

    public BrandStore()
    {
        brands = new HashMap<>();
    }

    private Brand tempBrand (int i) {
        Brand brand = new Brand(i, "Brand name " + i, "Brand desc " + i );
        return brand;
    }

    public void initialize() {
        for(int i = 0; i< 10; i++) {
            addBrand(tempBrand(i));
        }
    }
    public void addBrand(Brand brand) {
        brands.put(brand.getBrandid(), brand);
    }

    public void removeBrand(Brand brand) {

        brands.remove(brand.getBrandid());

    }

    public Brand getBrand(int id) {
        return brands.get(id);
    }
    public String viewBrandsAsString() {
        return brands.values().stream().map(i -> i.getName()).collect(Collectors.joining(",\n"));
    }
}
