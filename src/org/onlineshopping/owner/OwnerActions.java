package org.onlineshopping.owner;

public class OwnerActions {

    OwnerStores ownerStores;

    public OwnerActions(ProductStore productStore, BrandStore brandStore) {
        this.ownerStores = new OwnerStores(productStore, brandStore);
    }
    public void addBrand(int id, String name, String desc) {
        ownerStores.getBrands().addBrand(new Brand(id, name, desc));
    }
    public void addProduct(int id, String name,  int color,  int brandid, int quantity, float price, ProductType type) {
        ownerStores.getProducts().addProduct(new ProductEntity(id, name, color, ownerStores.getBrands().getBrand(brandid),
                quantity, price, type));
    }
    public String viewBrandsAsString () {
        return ownerStores.getBrands().viewBrandsAsString();
    }
    public String viewProductsAsString () {
        return ownerStores.getProducts().viewProductsAsString();
    }
}
