package org.onlineshopping.user;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Cart {
    ArrayList<Item> itemsInCart;
    public Cart()
    {
        itemsInCart = new ArrayList<>();
    }
    public void addToCart(Item item) {
        itemsInCart.add(item);
    }

    public void removeFromCart(Item item) {
        itemsInCart.remove(item);
    }

    public void updateCart(Item item) {
        itemsInCart.remove(item);
        itemsInCart.add(item);
    }

    public float    calculateTotal() {
        return itemsInCart.stream().map(i -> i.getProductEntity().getPrice() * i.getQuantity()).reduce((i, j) -> i + j).orElse((float) 0.0);
    }
    public String viewCartAsString() {
        return itemsInCart.stream().map(i -> i.getProductEntity().getName()).collect(Collectors.joining(",\n"));
    }
}
