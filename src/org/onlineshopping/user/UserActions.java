package org.onlineshopping.user;

import org.onlineshopping.owner.ProductEntity;

public class UserActions {
    UserStore store = new UserStore();

    public void addItemToCart(ProductEntity productEntity, int quantity) {
        store.getCart().addToCart(new Item(productEntity, quantity));
    }

    public float getCartTotal() {
        return store.getCart().calculateTotal();
    }

    public String viewCart() {
        return store.getCart().viewCartAsString();
    }
}
