package org.onlineshopping;

import org.onlineshopping.owner.BrandStore;
import org.onlineshopping.owner.OwnerActions;
import org.onlineshopping.owner.ProductStore;
import org.onlineshopping.owner.ProductType;
import org.onlineshopping.user.UserActions;

public class Main {

    public static void main(String[] args) {
	// write your code here
        BrandStore brands = new BrandStore();
        brands.initialize();

        ProductStore productStore = new ProductStore();
        productStore.initialize(1, brands.getBrand(1));
        productStore.initialize(11, brands.getBrand(2));

        productStore.initialize(21, brands.getBrand(3));
        productStore.initialize(31, brands.getBrand(4));

        OwnerActions ownerActions = new OwnerActions(productStore, brands);
        ownerActions.addBrand(11, "New Brand", "New Desc");

        ownerActions.addProduct(41, "Name 41", 41, 11,50, 200, ProductType.BAG);


        System.out.println(ownerActions.viewBrandsAsString());
        System.out.println(ownerActions.viewProductsAsString());
        UserActions userActions = new UserActions();
        userActions.addItemToCart(productStore.getProduct(11), 5);
        System.out.println(userActions.viewCart());
        System.out.println(userActions.getCartTotal());
    }
}
